<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node BACKGROUND_COLOR="#00bfff" CREATED="1442496226672" ID="ID_1394353631" MODIFIED="1442496386704" TEXT="Themes.org">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      --org-mode: WHOLE FILE
    </p>
  </body>
</html>
</richcontent>
<font NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226674" ID="ID_214961772" MODIFIED="1442496370263" POSITION="left" TEXT="HAB">
<font BOLD="true" NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226674" ID="ID_698451210" MODIFIED="1442496295659" TEXT="Flight planning">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226675" ID="ID_1799900475" MODIFIED="1442496295658" TEXT="Payload design">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226675" ID="ID_1929983230" MODIFIED="1442496295658" TEXT="Tracking">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226675" ID="ID_1397665242" MODIFIED="1442496295658" TEXT="APRS">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226675" ID="ID_1784295342" MODIFIED="1442496295657" TEXT="Fox hunting">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
</node>
<node CREATED="1442496226676" ID="ID_306193907" MODIFIED="1442496295656" TEXT="Data capture">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226676" ID="ID_1244847744" MODIFIED="1442496295656" TEXT="Command and control">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
</node>
<node CREATED="1442496226676" ID="ID_1937883758" MODIFIED="1442496375334" POSITION="left" TEXT="Construction">
<font BOLD="true" NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226677" ID="ID_83542960" MODIFIED="1442496295655" TEXT="Soldering skills/safety">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226677" ID="ID_1743892694" MODIFIED="1442496295655" TEXT="Basic electronics design">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226677" ID="ID_552636352" MODIFIED="1442496295654" TEXT="Building/Embedded">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226677" ID="ID_776095315" MODIFIED="1442496295654" TEXT="Platform comparison">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226678" ID="ID_917629185" MODIFIED="1442496295653" TEXT="Raspberry Pi">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226678" ID="ID_1707091388" MODIFIED="1442496295653" TEXT="Arduino">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226678" ID="ID_635382837" MODIFIED="1442496295652" TEXT="PIC">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
</node>
<node CREATED="1442496226678" ID="ID_1865590225" MODIFIED="1442496295652" TEXT="Sensors">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226679" ID="ID_215780695" MODIFIED="1442496295651" TEXT="User interface">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226679" ID="ID_796607923" MODIFIED="1442496295651" TEXT="Data comms">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
</node>
<node CREATED="1442496226679" ID="ID_1888506210" MODIFIED="1442496295650" TEXT="Building/Az-El">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226680" ID="ID_1149872748" MODIFIED="1442496295650" TEXT="Construction">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226680" ID="ID_820335340" MODIFIED="1442496295649" TEXT="Programming">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226680" ID="ID_1926555015" MODIFIED="1442496295649" TEXT="Internet data">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
</node>
</node>
<node CREATED="1442496226680" ID="ID_1974977146" MODIFIED="1442496379295" POSITION="right" TEXT="HF Operation">
<font BOLD="true" NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226681" ID="ID_634999361" MODIFIED="1442496295648" TEXT="Rag chewing">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226681" ID="ID_1738082488" MODIFIED="1442496295648" TEXT="DXing">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226681" ID="ID_512371778" MODIFIED="1442496295647" TEXT="Contesting">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226681" ID="ID_1452532872" MODIFIED="1442496295646" TEXT="NBEMS">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226682" ID="ID_1588182525" MODIFIED="1442496295646" TEXT="Traffic">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
</node>
<node CREATED="1442496226682" ID="ID_1917799270" MODIFIED="1442496382022" POSITION="right" TEXT="VHF Operation">
<font BOLD="true" NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226682" ID="ID_1188168929" MODIFIED="1442496295644" TEXT="Rag chewing">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226682" ID="ID_135684168" MODIFIED="1442496295644" TEXT="Repeaters">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226683" ID="ID_782724027" MODIFIED="1442496295643" TEXT="Packet">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226683" ID="ID_898004344" MODIFIED="1442496295642" TEXT="Satellite comms">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226683" ID="ID_673105827" MODIFIED="1442496295642" TEXT="ISS phone">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226684" ID="ID_625082042" MODIFIED="1442496295641" TEXT="ISS digipeat">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226684" ID="ID_734859681" MODIFIED="1442496295641" TEXT="Data decode">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
</node>
</node>
<node CREATED="1442496226684" ID="ID_470877810" MODIFIED="1442496384673" POSITION="right" TEXT="Emergency communications">
<font BOLD="true" NAME="Liberation Sans Narrow" SIZE="18"/>
<node CREATED="1442496226684" ID="ID_281008326" MODIFIED="1442496295638" TEXT="Local/State/Federal">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226685" ID="ID_1734721505" MODIFIED="1442496295637" TEXT="FEMA">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226685" ID="ID_943703504" MODIFIED="1442496295636" TEXT="Voice">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
<node CREATED="1442496226685" ID="ID_197598199" MODIFIED="1442496295635" TEXT="Data">
<font NAME="Liberation Sans Narrow" SIZE="18"/>
</node>
</node>
</node>
</map>
